FROM node:4

RUN apt update && \
    apt dist-upgrade -y;

RUN apt install -y git;

RUN mkdir -p /opt/gitlab-to-irc

WORKDIR /opt
RUN git clone https://framagit.org/bnjbvr/gitlab-to-irc

WORKDIR /opt/gitlab-to-irc
COPY ./config.js /opt/gitlab-to-irc/config.js

COPY ./docker-entrypoint.sh /opt/gitlab-to-irc/docker-entrypoint.sh

CMD /opt/gitlab-to-irc/docker-entrypoint.sh
