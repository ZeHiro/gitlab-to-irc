module.exports = {
    // IRC server to connect to.
    server: 'chat.freenode.net',

    // List of channels / people to report to.
    reports: {
        '#horsejs': ['push', 'merge_request'],
        'somebody': ['push', 'merge_request', 'issue', 'build']
    },

    projectUrl: "https://framagit.org/bnjbvr/kresus/",

    branches: ['master'],

    // IRC nick/names for the bot
    nick: 'gitlab-bot',
    userName: 'gitlab-bot',
    realName: 'gitlab-bot',

    // Secret as entered in the Gitlab Webhook instance.
    secret: '12345',

    // Port on which to run.
    port: 1337,

    // Network interface on which to run the webhook server.
    hostname: '0.0.0.0',

    // Instance of lstu to shorten links -- keep empty to not use.
    lstu: 'https://lstu.fr',

    // Whether the irc client debug messages should be printed.
    debug: true
}
