gitlab-to-irc
===

This is a simple Gitlab webhook that will transmit useful information to an IRC
channel or person.

how-to
===

- Rename `config.example.js` to `config.js` and change the values there.
- run `node ./index.js` from anywhere.
- fun and profit.

how-to (Docker)
===

- Rename `config.example.js` to `config.js` and change the values there.
- Modify the port mapping in `docker-run.sh`.
- `./run-docker.sh`
- fun and profit.

but this doesn't implement what i want
===

Merge requests are accepted :-)
